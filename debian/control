Source: flufl.testing
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Homepage: https://gitlab.com/warsaw/flufl.testing
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               pybuild-plugin-pyproject,
               python3-all,
               python3-setuptools,
Standards-Version: 3.9.8
Vcs-Git: https://salsa.debian.org/python-team/packages/flufl.testing.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/flufl.testing
Rules-Requires-Root: no

Package: python3-flufl.testing
Architecture: all
Depends: python3-pkg-resources,
         ${misc:Depends},
         ${python3:Depends},
Description: small collection of Python test helpers
 This package includes plugins for the following test tools: nose2, flake8.
 The plugins provide useful features (e.g. filtering tests based on a regular
 expression) and ensure some common coding styles (e.g. import order).  They
 can be enabled independently.
 .
 This package refactors code in the test suites of several other packages and
 will be used to eliminate the usual skew due to cargo culting.
 .
 This is the Python 3 version of the package.
